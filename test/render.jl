#========================================================================================================
    this file for rapidly making images and videos
========================================================================================================#

using ThorneHole, LinearAlgebra, StaticArrays, DiffEqPhysics, DifferentialEquations, TestImages, ImageView
using FileIO, VideoIO, BenchmarkTools

using ThorneHole: DnegWormhole, Spin1, raytrace, incidentmomentum, B, rotate!, sphere_surface_grid
using ThorneHole: interpolate_pixel, SphericalImage, loadimage, PinholeCamera
using ThorneHole: rotation, direction, raytracedimage, raytracedvideo!


outputpath(fname) = joinpath(@__DIR__,"..","site","_assets","output",fname)

saveimage(fname, img; kw...) = FileIO.save(outputpath(fname), img; kw...)
savevideo(fname, v; crf=28, kw...) = ThorneHole.savevideo(outputpath(fname), v; crf, kw...)
savevideo_hd(fname, v; crf=18, kw...)  = ThorneHole.savevideo(outputpath(fname), v; crf, kw...)


makewormhole() = DnegWormhole(1.0, 0.8, 0.2)

function image_at_infinity₊()
    σ₊ = SphericalImage(loadimage("earth.jpg"), 7*π/6 - 0.2)
end
function image_at_infinity₋()
    σ₋ = SphericalImage(loadimage("gaia.jpg"))
end

function cam(ℓ=-7.0, n₁=1.0)
    C = PinholeCamera(3.2, 3.2*(9/16), 1280, 720,
                      [0.0, ℓ, π/2, 0.0],
                      [n₁, 0.0, 0.0],
                     )
end
function camhd(ℓ=-7.0, n₁=1.0)
    C = PinholeCamera(3.2, 3.2*(9/16), 1920, 1080,
                      [0.0, ℓ, π/2, 0.0],
                      [n₁, 0.0, 0.0],
                     )
end

function minknowki(C=cam(), σ₋=image_at_infinity₋(), σ₊=image_at_infinity₊(); Δt=100.0)
    raytracedimage(C, σ₋, σ₊, ThorneHole.MinkowskiWormhole(); Δt)
end

function wormhole(C=camhd(), σ₋=image_at_infinity₋(), σ₊=image_at_infinity₊(); W=makewormhole(),
                  Δt=100.0, remove_singularity=true)
    raytracedimage(C, σ₋, σ₊, W; Δt, remove_singularity)
end

function video(C=camhd(), σ₋=image_at_infinity₋(), σ₊=image_at_infinity₊(); W=makewormhole(),
               Δt=100.0, remove_singularity=true)
    raytracedvideo!(C, σ₋, σ₊, 30*10; W, Δt, remove_singularity)
end

function rotationvideo(C=camhd(-7.0, 1.0), σ₋=image_at_infinity₋(), σ₊=image_at_infinity₊(); W=makewormhole(),
                       Δt=100.0, δϕ=π, remove_singularity=true, n=30*12)
    γ = ThorneHole.trajectory_combined(C, -C.x[2], δϕ, n)
    raytracedvideo!(C, σ₋, σ₊, γ; W, Δt, remove_singularity)
end

# have run a 2s, 4s, 2s
function rotationvideo2(C=camhd(-7.0, -1.0), σ₋=image_at_infinity₋(), σ₊=image_at_infinity₊(); W=makewormhole(),
                        Δt=100.0, remove_singularity=true)
    γ₁ = ThorneHole.trajectory_rotate(C, π, 30*2)
    v₁ = raytracedvideo!(C, σ₋, σ₊, γ₁; W, Δt, remove_singularity, reset=false)
    γ₂ = ThorneHole.trajectory_translate(C, 14.0, 30*4)
    v₂ = raytracedvideo!(C, σ₋, σ₊, γ₂; W, Δt, remove_singularity, reset=false)
    γ₃ = ThorneHole.trajectory_rotate(C, π, 30*2)
    v₃ = raytracedvideo!(C, σ₋, σ₊, γ₃; W, Δt, remove_singularity, reset=true)
    [v₁; v₂; v₃]
end

