# ThorneHole

See my [blog post](https://expandingman.gitlab.io/ThorneHole.jl/).

Ray-traced rendering in the wormhole metric defined in [this
paper](https://arxiv.org/abs/1502.03809).

