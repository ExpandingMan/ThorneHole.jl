
#========================================================================================================
    The general procedure for removing singularities is:

    1. Compute rotation to remove singularity.
    2. Write this as $R(\psi)R_{z}(\phi)R_{y}(\theta) = R_{z}(\varphi)R_{y}(\vartheta) in SU(2)
    3. Solve for $R(\psi)$.
    4. Apply $R(\psi)$ everywhere.
    5. Solve.
    5. Apply $R^{-1}(\psi)$ everywhere.
========================================================================================================#

#========================================================================================================
    WARNING: to keep this from taking forever, first we will concentrate on removing the singularity
    for points that already lie on the x axis

    The shortest route to making this more general would probably be to always first rotate
    such that we lie on the x-axis.
========================================================================================================#

blochspinor(θ::Real, ϕ::Real) = @SVector [cos(θ/2), ℯ^(𝒾*ϕ)*sin(θ/2)]

function blochrotatex(φ::Real)
    @SMatrix [cos(φ/2)     -𝒾*sin(φ/2)
              -𝒾*sin(φ/2)  cos(φ/2)   ]
end
function blochrotatey(φ::Real)
    @SMatrix [cos(φ/2)  -sin(φ/2)
              sin(φ/2)  cos(φ/2) ]
end
function blochrotatez(φ::Real)
    @SMatrix [ℯ^(-𝒾*φ/2)  0
              0           ℯ^(𝒾*φ/2)]
end

function anglesfromspinor(ψ::AbstractVector)
    θ = 2*acos(abs(ψ[1]))
    ϕ = angle(ψ[2]/ψ[1])
    θ, ϕ
end

function default_rotate_angles(θ, ϕ; R=blochrotatex(-π/2))
    ψ = blochspinor(θ, ϕ)
    ψ′ = R*ψ
    anglesfromspinor(ψ′)
end

default_rotate_angles_inverse(θ, ϕ; R=blochrotatex(π/2)) = default_rotate_angles(θ, ϕ; R)

Base.inv(::typeof(default_rotate_angles)) = default_rotate_angles_inverse

function singularity_removal_simple(α::Spin1)
    n̂′ = @SVector [α.n̂[1], α.n̂[3], -α.n̂[2]]
    ϑ, φ = default_rotate_angles(α.x[3], α.x[4])
    x′ = @SVector [α.x[1], α.x[2], ϑ, φ]
    Spin1(x′, n̂′)
end


should_use_simple_removal(n̂::AbstractVector) = n̂[2]^2 > n̂[3]^2
should_use_simple_removal(α::Spin1) = should_use_simple_removal(α.n̂)
