module ThorneHole

using StaticArrays, LinearAlgebra, DiffEqPhysics, ImageCore, Rotations
import FileIO, VideoIO, ThreadsX, ProgressMeter

using ProgressMeter: Progress, next!, @showprogress

# this is a heavy dependecy but who really cares since nothing is ever likely to have ThorneHole as a dependency
using DifferentialEquations

const ∞ = Inf
const 𝒾 = im
const mod2π = mod2pi

# this allows us to change a bunch of statically typed stuff, especially in case we use GPU
const Float = Float64

const Vector3 = SVector{3,Float}
const Vector4 = SVector{4,Float}

include("utils.jl")
include("metric.jl")
include("raytrace.jl")
include("singularity_removal.jl")
include("images.jl")
include("ensemble.jl")


# Coordinate basis vectors have components:
#   t, ℓ, θ, ϕ
#   (above basis is used unless otherwise specified)

"""
    sphere_surface_grid(Θ, Φ, n::Integer=100)

Create a grid of evenly spaced points on a sphere with polar angles between `Θ` and azimuthal angles between
`Φ` (where these are, for example, 2-element tuples).  `n` polar coordinates will be used, but the number of
azimuthal coordinates is determined by trigonometry.  Because of this, it doesn't make sense to return the result
as a matrix, and instead it is returned as a `Vector` of 2-element `SVector`s giving the angular coordinates.
"""
function sphere_surface_grid(Θ, Φ, n::Integer=100)
    γ = SVector{2,Float}[]
    θ = first(Θ)
    δθ = (last(Θ) - θ)/n
    for i ∈ 1:n
        ϕ = first(Φ)
        nϕ = ceil(Int, n*sin(θ))
        δϕ = (last(Φ) - ϕ)/nϕ
        for j ∈ 1:nϕ
            push!(γ, @SVector [θ, ϕ])
            ϕ += δϕ
        end
        θ += δθ
    end
    γ
end


end
