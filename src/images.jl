
function loadimage(fname::AbstractString)
    FileIO.load(joinpath(dirname(pathof(ThorneHole)),"..","site","_assets",fname))
end

"""
    savevideo(fname, v; kw...)

This is here because the [options](https://ffmpeg.org/ffmpeg-codecs.html#Codec-Options) available
for writing videos with libffmpeg (VideoIO.jl) are overwhelming, and it is easy to write videos that
most software can't read, or have so little compression that a few seconds are 100 MB.

So, I provide this function for myself so I can remember what the hell to do.  Better documentation
and perhaps more convenience functions are needed in VideoIO, see
[this issue](https://github.com/JuliaIO/VideoIO.jl/issues/327).

Note that larger values of `crf` and `preset` give *more compressed* videos.

For large videos, good settings are `crf=18, preset="veryslow"`.

For small videos good settings are `crf=28, preset="veryslow"`.
"""
function savevideo(fname::AbstractString, v; framerate::Integer=30, crf::Integer=28, preset="veryslow", kw...)
    VideoIO.save(fname, v; target_pix_fmt=VideoIO.AV_PIX_FMT_YUV420P, framerate,
                 encoder_options=(;crf, preset))
end


"""
    sphere2cylinder(Θ)

Converts coordinates ``(θ, ϕ)`` on a 2-sphere to coordinates ``(x, y)`` on an unrolled cylinder (rectangle).

The corners of the rectangle correspond to ``(0, 0), (0, 1), (1, 0), (1, 1)`` (so one should simply multiply to
convert coordinates).

Apparently this is known as the [Lambert cylindrical projection](https://en.wikipedia.org/wiki/Lambert_cylindrical_equal-area_projection)
(though I really don't see how this simple projection can be named for somebody).
"""
sphere2cylinder(Θ::AbstractVector) = @SVector [Θ[2] / (2π), (1 + sin(Θ[1]))/2]

"""
    cylinder2sphere(X)

Converts coordinates ``(x, y)`` on an unrolled cylinder (rectangle) to ``(θ, ϕ)`` on a sphere.
"""
cylinder2sphere(X::AbstractVector) = @SVector [asin(2.0*X[2] - 1), 2.0*π*X[1]]

"""
    interpolate_pixel(img::AbstractMatrix, x::Real, y::Real)

Given an image `img`, get the pixel which is closest to the point ``(x, y)`` on the image,
where ``x`` and ``y`` follow the conventional right-handed coordinate convention.

For example
```
img = [1 2
       3 4]

interpolate_pixel(img, 0, 0) == 3
interpolate_pixel(img, 0, 1) == 1
interpolate_pixel(img, 1, 0) == 4
interpolate_pixel(img, 1, 1) == 2
```
"""
function interpolate_pixel(img::AbstractMatrix, x::Real, y::Real)
    α, β = axes(img)
    idx1 = round(Int, (1.0 - y)*(length(α) - 1) + α[1])
    idx2 = round(Int, x*(length(β) - 1) + β[1])
    img[idx1, idx2]
end

"""
    SphericalImage

An image which can be indexed by spherical coordinates ``(\\theta, \\phi)`` where ``\\theta \\in [-\\pi/2, \\pi/2]`` and
``\\phi \\in [0, 2\\pi)``.

The upper edge of the rectangular image appears at the ``\\theta=0`` pole and the lower edge at the ``\\theta=\\pi`` pole.
The horizontal center of the rectangular image is at ``\\theta=\\pi``.  These conventions have us looking at the center
in the image when looking into the wormhole from the ``+\\ell`` side when the image is aligned with the directional
coordinate convetion used for `Spin1` ("camera local sky" of the paper).

The azimuthal coordinate will be shifted by `δϕ`.

No transformation is performed on the image itself, rather, it is assumed that the image is an equirectangular map so that
there is a linear relation between each angular coordinate and its corresponding image matrix index.
"""
struct SphericalImage{𝒯<:AbstractMatrix}
    image::𝒯
    δϕ::Float
end

SphericalImage(img::AbstractMatrix, δϕ::Real=0.0) = SphericalImage{typeof(img)}(img, δϕ)

function Base.getindex(σ::SphericalImage, θ::Real, ϕ::Real)
    x = mod((ϕ - σ.δϕ)/(2*π), 1.0)
    y = 1.0 - θ/π
    interpolate_pixel(σ.image, float(x), float(y))
end
Base.getindex(σ::SphericalImage, Θ::AbstractVector) = getindex(σ, Θ[1], Θ[2])

# this is useful for debugging
function Base.view(σ::SphericalImage, δθ, δϕ; res=(1000,1000))
    δθ = range(first(δθ), stop=last(δθ), length=res[1])
    δϕ = compactrange(float(first(δϕ)), last(δϕ), n=res[2])
    map(Iterators.product(δθ, δϕ)) do (θ, ϕ)
        σ[θ, ϕ]
    end
end

"""
    PinholeCamera

An ideal pinhole camera (i.e. with a pinhole of 0 radius) with an imager of height `Δy` and width `Δx`.  The dimensions
of the imager are given in units of the camera's focal distance (distance to imaging screen) so that the focal distance
does not need to be provided.

When we refer to the camera's local frame we take the camera to be pointing in the ``+z`` direction, with the camera's
vertical direction corresponding to ``+y``.
"""
mutable struct PinholeCamera
    Δx::Float  # units normalized such that f = 1
    Δy::Float

    xres::Int
    yres::Int

    x::Vector4
    n̂::Vector3  # direction of camera, we call this ẑ for camera's local coordinates, given in global spherical coordinates
    # NOTE: we always take the "up" direction of the camera to be the ``y`` direction in its local frame
end

function move!(C::PinholeCamera, α::Spin1)
    C.x, C.n̂ = α.x, α.n̂
    C
end

function shift!(C::PinholeCamera, δℓ::Real)
    C.x = SVector{4}(C.x[1], C.x[2]+ℓ, C.x[3], C.x[4])
    C
end
function shift!(C::PinholeCamera, δℓ::Real, δϕ::Real)
    shift!(C, δℓ)
    C.n̂ = RotZ(δϕ)*C.n̂
    C
end

imager_points_x(C::PinholeCamera) = range(-C.Δx/2, stop=C.Δx/2, length=C.xres)
imager_points_y(C::PinholeCamera) = range(-C.Δy/2, stop=C.Δy/2, length=C.yres)

# note that this is backwards because of matrix conventions
imager_points(C::PinholeCamera) = Iterators.product(reverse(imager_points_y(C)), imager_points_x(C))

function imager_point(C::PinholeCamera, i::Integer, j::Integer)
    xpoints = imager_points_x(C)
    ypoints = imager_points_y(C)
    xpoints[i], ypoints[j]
end

LinearAlgebra.norm(C::PinholeCamera) = C.n̂

axis_vertical(C::PinholeCamera) = C.ŷ

axis_horizontal(C::PinholeCamera) = C.n̂ × C.ŷ  # this is parity flipped

"""
    rotation2frame(C::PinholeCamera)

Returns a rotation operator that rotates vectors in the spherical coordinates into the frame of the camera.
In the camera's local frame, we take the direction of the camera to be ``+z``.

For example, for a camera pointing in the ``+\\ell`` direction, the ``\\theta`` basis vector becomes the ``y``
basis vector and the ``\\phi`` basis vector becomes the ``-x`` basis vector.
"""
rotation(C::PinholeCamera) = rotation(C.n̂, @SVector [0.0, 0.0, 1.0])

"""
    direction(C::PinholeCamera, x::Real, y::Real)

Compute the direction corresponding to the location at `(x, y)` on the camera's imager.

Note that this does an extra parity flip in addition to the one that would normally be induced by a pinhole camera so
that, for example, a camera pointing along ``+\\ell`` has its ``y > 0`` points in the ``+\\theta`` direction.
"""
function direction(C::PinholeCamera, x::Real, y::Real)
    μ = SVector{3,Float}(x, y, 1.0)
    μ = μ ./ norm(μ)
    inv(rotation(C))*μ
end

Spin1(C::PinholeCamera, x::Real=0.0, y::Real=0.0) = Spin1(C.x, direction(C, x, y))

"""
    raytracedimage(C::PinholeCamera, σ₋::SphericalImage, σ₊::SphericalImage,
                   W=default_wormhole(); Δt=100.0, remove_singularity=true)

Make a raytraced image of the camera's view.  The image at `ℓ→∞` is `σ₊` and the image at
`ℓ→-∞` is `σ₋`.
"""
function raytracedimage(C::PinholeCamera, σ₋::SphericalImage, σ₊::SphericalImage,
                        W::AbstractWormhole=default_wormhole(); Δt::Real=100.0, remove_singularity::Bool=true)
    ThreadsX.map(imager_points(C)) do (y, x)
        n = direction(C, x, y)
        a, θ, ϕ = raytrace(Spin1(C.x, n), Δt, W; remove_singularity)
        (a > 0 ? σ₊ : σ₋)[θ, ϕ]
    end
end

"""
    trajectory!(𝒻, C::PinholeCamera, γ)

Move the camera `C` along the trajectory `γ` calling `𝒻(C, i, γₙ)` at each step where
`i` is the iteration number.
"""
function trajectory!(𝒻, C::PinholeCamera, γ; reset::Bool=true)
    x₀, n̂₀ = C.x, C.n̂
    # NOTE: as expected, threads don't really speed this up
    map(enumerate(γ)) do (i, α)
        move!(C, α)
        𝒻(C, i, α)
    end
    reset && ((C.x, C.n̂) = (x₀, n̂₀))  # otherwise I'll just forget it all the time
    C
end

function trajectory_combined(C::PinholeCamera, ℓ₁::Real, δϕ::Real, n::Integer=60)
    x = copy(C.x)
    n̂ = copy(C.n̂)
    ℓs = range(x[2], stop=ℓ₁, length=n)
    n = (RotY(φ)*n̂ for φ ∈ range(0.0, stop=δϕ, length=n))
    (Spin1(SVector{4}(x[1], l, x[3], x[4]), ν) for (l, ν) ∈ zip(ℓs, n))
end

function trajectory_rotate(C::PinholeCamera, δϕ::Real, n::Integer=60)
    n̂ = copy(C.n̂)
    n = (RotY(φ)*n̂ for φ ∈ range(0.0, stop=δϕ, length=n))
    (Spin1(C.x, ν) for ν ∈ n)
end
function trajectory_translate(C::PinholeCamera, δℓ::Real, n::Integer=60)
    x = copy(C.x)
    n̂ = copy(C.n̂)
    (Spin1(SVector{4}(x[1], l, x[3], x[4]), n̂) for l ∈ range(x[2], stop=x[2]+δℓ, length=n))
end

"""
    raytracedvideo!

Make a raytraced video by calling `raytracedimage` in a loop.  The camera will be moved along the trajectory
`trajectory` lying along the x-axis.

**WARN**: For now please just always put the camera on the x-axis.
"""
function raytracedvideo!(C::PinholeCamera, σ₋::SphericalImage, σ₊::SphericalImage,
                         γ=range(C.x[2], stop=-C.x[2], length=60);
                         W::AbstractWormhole=default_wormhole(), Δt::Real=100.0, remove_singularity::Bool=true,
                         reset::Bool=true,
                        )
    v = Vector{Matrix{RGB{N0f8}}}(undef, length(γ))
    pb = Progress(length(γ), dt=0.0, desc="raytracing... ", color=:blue, showspeed=true)
    next!(pb, step=0) # otherwise it just won't show up until first iteration is done
    trajectory!(C, γ; reset) do C, i, ℓ
        v[i] = raytracedimage(C, σ₋, σ₊, W; Δt, remove_singularity)
        next!(pb)
    end
    v
end
function raytracedvideo!(C::PinholeCamera, σ₋::SphericalImage, σ₊::SphericalImage, n::Integer;
                         W::AbstractWormhole=default_wormhole(), Δt::Real=100.0, remove_singularity::Bool=true,
                         reset::Bool=true,
                        )
    raytracedvideo!(C, σ₋, σ₊, range(C.x[2], stop=-C.x[2], length=n); W, Δt, remove_singularity, reset)
end

