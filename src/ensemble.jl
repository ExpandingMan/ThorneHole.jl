#========================================================================================================
    TODO:
    the below essentially does the same thing as `raytracedimage` but currently is running into
    a bizarre and infuriating heisenbug,
    see [this issue](https://github.com/SciML/SciMLBase.jl/issues/81)
========================================================================================================#

Base.CartesianIndices(C::PinholeCamera) = CartesianIndices((C.xres, C.yres))

function fromproblemindex(::Type{Spin1}, C::PinholeCamera, i::Integer)
    idx = CartesianIndices(C)[i]
    x, y = imager_point(C, idx[1], idx[2])
    Spin1(C, x, y)
end


function generate_problem(C::PinholeCamera, prob, i, repeat)
    α = fromproblemindex(Spin1, C, i)
    p = incidentmomentum(α)
    remake(prob, u0=ArrayPartition(p, α.x))
end

function DiffEqPhysics.HamiltonianProblem(C::PinholeCamera, Δt::Real=100.0, W::AbstractWormhole=default_wormhole())
    ThreadsX.map(imager_points(C)) do (y, x)
        HamiltonianProblem(Spin1(C, x, y), Δt, W)
    end
end

function SciMLBase.EnsembleProblem(C::PinholeCamera, Δt::Real=100.0, W::AbstractWormhole=default_wormhole())
    prob = HamiltonianProblem(Spin1(C), Δt, W)
    EnsembleProblem(prob, safetycopy=false,
                    prob_func=(prob, i, repeat) -> generate_problem(C, prob, i, repeat),
                    output_func=(s, i) -> (last(s.u), false),
                   )
end

function SciMLBase.solve(C::PinholeCamera, method=EnsembleThreads(), Δt::Real=100.0, W::AbstractWormhole=default_wormhole();
                         batch_size::Integer=10^4)
    prob = EnsembleProblem(C, Δt, W)
    solve(prob, method; trajectories=C.xres*C.yres, batch_size, save_everystep=false, save_start=false, save_end=true)
end
