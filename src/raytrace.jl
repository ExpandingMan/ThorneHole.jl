
# note that here `p` is the *covariant* momentum, but `x` is the *contravariant* position
"""
    H(x, p, W=default_wormhole())

Compute the Hamiltonian at (canonical) position ``x`` and momentum ``p``.  Note that here ``x`` is the *contravariat*
position while ``p`` is the covariant momentum, for no better reason that that this is how their components
appear in the Hamiltonian.
"""
function H(x::AbstractVector, p::AbstractVector, W::AbstractWormhole=default_wormhole())
    r̂ = r(x, W)
    # this uses pₜ = -1 which sets t to be the affine parameter
    h = -1.0 + p[2]^2 + p[3]^2/r̂^2 + p[4]^2 / (r̂*sin(x[3]))^2
    h/2.0
end

"""
    Spin1

A spin-1 object parameterized by position `x` and spatial direction `n̂`.
"""
struct Spin1
    x::Vector4

    # direction... ϑ = π/2, φ = π is looking *into* the wormhole from the +ℓ
    # this might seem like a bizarre convention, but I think it was chosen so that (ϑ,φ) = (0,0) is at a corner
    # of the image, which seems convenient for this application

    # this is the direction given in the e_ℓ, e_θ, e_ϕ basis
    n̂::Vector3
end

# this constructor has the helicity pointing *into* the wormhole
function Spin1(t::Real, ℓ::Real, θ::Real=π/2, ϕ::Real=π, n̂::AbstractVector=@SVector [-1.0, 0.0, 0.0])
    x = @SVector [t, ℓ, θ, ϕ]
    Spin1(x, n̂)
end

Base.position(α::Spin1) = α.x

r(α::Spin1, W::AbstractWormhole=default_wormhole()) = r(α.x, α.W)

# the impact parameter is a constant of motion which follows from p² = 0
# this is useful for checking solutions
B(x::AbstractVector, p::AbstractVector) = √(p[3]^2 + p[4]^2/sin(x[3])^2)



# NOTE: major difference from the paper is that his y is along ϕ but mine is along θ
# ... just seems less confusing this way

# gives the standard coordinate basis *covariant* momentum vector for a ray incident at the spin-1 object
# this is the 4-momentum of the photon at the time of the camera
function incidentmomentum(α::Spin1, W::AbstractWormhole=default_wormhole())
    n̂ = -α.n̂  # note - sign is here, don't lose track of it below
    r̃ = r(α.x, W)
    pℓ = n̂[1]  # the incoming ray is anti-parallel to n̂
    pθ = r̃*n̂[2]
    pϕ = r̃*sin(α.x[3])*n̂[3]
    # again pₜ = -1 is a convention that sets t to the affine parameter
    @SVector [-1.0, pℓ, pθ, pϕ]
end

function DiffEqPhysics.HamiltonianProblem(x::AbstractVector, p::AbstractVector, Δt::Real=100.0,
                                          W::AbstractWormhole=default_wormhole())
    # DiffEqPhysics requires the 4-argument form below where Ξ is params
    HamiltonianProblem(p, x, (x[1], x[1] - Δt)) do p′, q′, Ξ, t
        H(q′, p′, W)
    end
end
function DiffEqPhysics.HamiltonianProblem(α::Spin1, Δt::Real=100.0, W::AbstractWormhole=default_wormhole())
    p = incidentmomentum(α, W)
    HamiltonianProblem(α.x, p, Δt)
end

function DiffEqPhysics.solve(x::AbstractVector, p::AbstractVector, Δt::Real=100.0, W::AbstractWormhole=default_wormhole())
    solve(HamiltonianProblem(x, p, Δt, W))
end
function DiffEqPhysics.solve(α::Spin1, Δt::Real=100.0, W::AbstractWormhole=default_wormhole())
    solve(HamiltonianProblem(α, Δt, W))
end



function raytrace(::Type{Vector}, α::Spin1, Δt::Real=100.0, W::AbstractWormhole=default_wormhole();
                  remove_singularity::Bool=true,
                 )
    sr = remove_singularity && should_use_simple_removal(α)
    sr && (α = singularity_removal_simple(α))
    s = solve(α, Δt, W)
    u = last(s.u)
    x = regularizeangles(u.x[2])  # this is a weird unpacking from their weird ArrayPartition thing
    p = u.x[1]
    if sr
        θ, ϕ = inv(default_rotate_angles)(x[3], x[4])
        ϕ = mod2π(ϕ)  # make sure we get a proper azimuth
        x = @SVector [x[1], x[2], θ, ϕ]
    end
    x = regularizeangles(x)
    # WARNING!! currently we are not bothering to fix the momentum since we are not using it
    x, p
end

"""
    raytrace(α::Spin1, Δt=100.0, W=default_wormhole())

Trace the ray incident on the spin-1 object (i.e. which meets it head-on with momentum anti-parallel to the direction
of the object) back to when the affine parameter was smaller by `Δt`.  Our default of `Δt=100.0` is meant to serve
as a proxy for infinity for metrics we typically use.

The returned value is ``(u, \\theta, \\phi)`` where ``u \\in \\{\\pm 1\\}`` indicates which end of the wormhole you are
in and ``\\theta`` and ``\\phi`` are the angular coordinates there in the "Dneg" metric.

If you want the ``\\ell`` value or anything else, use `raytrace(Vector, α, Δt, W)`.
"""
function raytrace(α::Spin1, Δt::Real=100.0, W::AbstractWormhole=default_wormhole();
                  remove_singularity::Bool=true,
                 )
    x, p = raytrace(Vector, α, Δt, W; remove_singularity)
    Int(sign(x[2])), x[3], x[4]
end

"""
    raytrace(α::Spin1, Δt::Real, W::MinkowskiWormhole)

Perform raytracing in Minkowski space.  Of course, this just uses an analytic solution and doesn't have to solve anything,
but there are a bunch of coordinate transformations so I'm not sure how efficient it really is (though rough benchmarking
makes it look pretty efficient).
"""
function raytrace(α::Spin1, Δt::Real, W::MinkowskiWormhole; remove_singularity::Bool=true)
    R = rotation_spherical2cartesian(view(α.x, 2:4))
    x₀ = spherical2cartesian(view(α.x, 2:4))
    x = -(R*α.n̂) .* (α.x[1] - Δt) + x₀
    ξ = cartesian2spherical(x)
    1, ξ[2], mod2π(ξ[3])
end


