
abstract type AbstractWormhole end

"""
    MinkowskiWormhole

It is useful to test certain things in Minkowski space for sanity checks.

The name of this struct is a bit of a joke.
"""
struct MinkowskiWormhole <: AbstractWormhole end

r(ℓ::Real, W::MinkowskiWormhole) = ℓ

"""
    SharpWormhole

A wormhole with sharp boundaries as given in the first part of the paper.
"""
struct SharpWormhole <: AbstractWormhole
    ρ::Float
    a::Float
end
# default params
SharpWormhole() = SharpWormhole(1.0, 0.5)

r(ℓ::Real, W::SharpWormhole) = abs(ℓ) ≤ W.a ? W.ρ : abs(ℓ) - W.a + W.ρ

"""
    DnegWormhole

The, rather artificial, but convenient wormhole metric that is referred to in the bulk of the paper.
"""
struct DnegWormhole <: AbstractWormhole
    ρ::Float
    a::Float
    M::Float
end
DnegWormhole() = DnegWormhole(1.0, 0.5, 0.5)

# it just makes life easier to have this around as a default
const DEFAULT_WORMHOLE = Ref{DnegWormhole}(DnegWormhole())

"""
    default_wormhole()

It is convenient to have a default wormhole metric because the vast majority of the time we only want to deal with one
specific metric.  This function returns that default wormhole.
"""
default_wormhole() = DEFAULT_WORMHOLE[]

"""
    default_wormhole!(W)

Set the default wormhole.
"""
default_wormhole!(W::DnegWormhole) = (DEFAULT_WORMHOLE[] = W)

# this is called x in the paper, but we call it ξ to avoid confusion with position vectors
"""
    ξ(ℓ, W=default_wormhole())

Compute the dimensionless quantity which is called ``x`` in the paper but which we call ``\\xi`` to avoid confusion with
position vectors, which we conventionally call ``x``.
"""
ξ(ℓ::Real, W::DnegWormhole=default_wormhole()) = 2*(abs(ℓ) - W.a)/(π*W.M)

"""
    r(ℓ::Real, W=default_wormhole())
    r(x::AbstractVector, W=default_wormhole())

Compute the quantity referred to as ``r`` in the paper as a function of the radial coordinate ``\\ell`` and which
is the square root of the coefficient of the angular part of the metric.
"""
function r(ℓ::Real, W::DnegWormhole)
    if abs(ℓ) < W.a
        W.ρ
    else
        ξ̂ = ξ(ℓ, W)
        W.ρ + W.M*(ξ̂*atan(ξ̂) - log(1.0 + ξ̂^2)/2.0)
    end
end
r(x::AbstractVector, W::DnegWormhole) = r(x[2], W)

r(x::AbstractVector, W::AbstractWormhole=default_wormhole()) = r(x[2], W)
