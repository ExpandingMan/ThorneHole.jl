
function compactrange(a, b; n=100, period=2*π)
    v = Vector{typeof(a)}(undef, n)
    x = a
    δx = (b - a)/n
    for i ∈ 1:n
        x  = mod(x + δx, period)
        v[i] = x
    end
    v
end

"""
    regularizeangles(θ, ϕ)

Attempt to regularize angular coordinates with invalid polar angles.  This is an attempt to tame the behavior of the
spherical coordinate singularities and should make sence for small displacements from the poles.

For example, if we start at `θ = 0` and a small displacement takes this to `θ = 0.001`, then at `ϕ = 0`
we will have `regularizeangles(θ, ϕ) ≈ (0.001, π)`.
"""
function regularizeangles(θ::Real, ϕ::Real)
    α, β = divrem(float(θ), π)
    θ, ϕ = if iseven(Int(α))
        β < 0 ? (-β, ϕ + π) : (β, ϕ)
    else
        β < 0 ? (-β, ϕ) : (π - β, ϕ+π)
    end
    θ, mod2π(ϕ)
end
function regularizeangles(x::AbstractVector)  # this method for a 4-vector
    θ, ϕ = regularizeangles(x[3], x[4])
    @SVector [x[1], x[2], θ, ϕ]
end

"""
    atan2π(y, x)

Same as `atan(y, x)` but converts the range to the conventional `[0, 2π)`.
"""
atan2π(y::Real, x::Real) = mod2π(atan(y, x))

"""
    rotationaxis(x, y)

Returns the (normalized) axis of the rotation that rotates the 3-vector `x` into the 3-vector `y`.
"""
function rotationaxis(x::AbstractVector, y::AbstractVector)
    n = x × y
    n ./ norm(n)
end

"""
    rotationangle(x, y)

Returns the angle between the vectors `x` and `y`.
"""
rotationangle(x::AbstractVector, y::AbstractVector) = acos((x⋅y)/(norm(x)*norm(y)))

"""
    rotation(x, y)

Return the rotation that rotates the 3-vector `x` into the 3-vector `y`.
"""
function rotation(x::AbstractVector, y::AbstractVector)
    n = rotationaxis(x, y)
    # the below happens if x and y are parallel so we give a type compatible identity
    any(isnan, n) && return AngleAxis{eltype(n)}(0.0, 1.0, 0.0, 0.0)
    θ = rotationangle(x, y)
    AngleAxis{eltype(n)}(θ, n...)
end

"""
    cartesian2spherical(x)

Return a spherical 3-vector ``(r, \\theta, \\phi)`` given the Cartesian coordinate vector `x` (same origin).
"""
function cartesian2spherical(x::AbstractVector)
    r = norm(x)
    θ = acos(x[3]/r)
    ϕ = atan2π(x[2], x[1])
    @SVector [r, θ, ϕ]
end

"""
    spherical2cartesian(ξ)

Return a Cartesian 3-vector given the spherical coordinate-basis vector ``(r, \\theta, \\phi)``.
"""
function spherical2cartesian(ξ::AbstractVector)
    x = ξ[1]*sin(ξ[2])*cos(ξ[3])
    y = ξ[1]*sin(ξ[2])*sin(ξ[3])
    z = ξ[1]*cos(ξ[2])
    @SVector [x, y, z]
end

"""
    rotation_spherical2cartesian(x)

Construct the matrix which transforms the spherical ``(r, \\theta, \\phi)`` basis into the cartesian basis (aligned
such that ``\\theta=0`` points in the ``+z`` direction) at the (3-vector) point `x`.
"""
function rotation_spherical2cartesian(x::AbstractVector)
    θ, ϕ = x[2], x[3]
    @SMatrix [sin(θ)*cos(ϕ)  cos(θ)*cos(ϕ)  -sin(ϕ)
              sin(θ)*sin(ϕ)  cos(θ)*sin(ϕ)  cos(ϕ)
              cos(θ)         -sin(θ)        0.0    ]
end
