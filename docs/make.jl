using ThorneHole
using Documenter

DocMeta.setdocmeta!(ThorneHole, :DocTestSetup, :(using ThorneHole); recursive=true)

makedocs(;
    modules=[ThorneHole],
    authors="Expanding Man <savastio@protonmail.com> and contributors",
    repo="https://gitlab.com/ExpandingMan/ThorneHole.jl/blob/{commit}{path}#{line}",
    sitename="ThorneHole.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://ExpandingMan.gitlab.io/ThorneHole.jl",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
