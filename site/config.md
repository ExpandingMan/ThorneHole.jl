<!--
Add here global page variables to use throughout your
website.
The website_* must be defined for the RSS to work
-->
@def website_title = "ThorneHole.jl"
@def website_descr = "a fun and simple general relativistic raytracing project"
@def website_url   = "https://expandingman.gitlab.io/ThorneHole.jl/"

@def prepath = "ThorneHole.jl"

@def author = "Expanding Man"

@def mintoclevel = 2

<!--
Add here files or directories that should be ignored by Franklin, otherwise
these files might be copied and, if markdown, processed by Franklin which
you might not want. Indicate directories by ending the name with a `/`.
-->
@def ignore = ["node_modules/", "franklin", "franklin.pub"]

<!--
Add here global latex commands to use throughout your
pages. It can be math commands but does not need to be.
For instance:
* \newcommand{\phrase}{This is a long phrase to copy.}
-->
\newcommand{\R}{\mathbb R}
\newcommand{\scal}[1]{\langle #1 \rangle}

\newcommand{\video}[1]{
~~~
<video controls width="100%">
    <source src="!#1"
            type="video/mp4">
    Sorry, your browser doesn't support embedded videos.
</video>
~~~
}

\newcommand{\figenv}[2]{
~~~
<figure style="text-align:center;">
<img src="!#2" alt="#1"/>
<figcaption>#1</figcaption>
</figure>
~~~
}
