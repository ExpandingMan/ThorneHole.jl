# ThorneHole.jl Outputs

The following are the default parameters for the wormhole unless otherwise specified:
- `ρ = 1`
- `a = 4/5`
- `M = 1/5`

Images or videos with non-default parameters have them listed in the filename.
Parameters which are not mentioned have default values.
